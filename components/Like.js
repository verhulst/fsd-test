import { useState, useEffect } from 'react';
// import axios from "axios"
import useSecurity from '../useSecurity'

const Like = ({ productId }) => {
    const [likes, setLikes] = useState(null);
    const { userId, token } = useSecurity();
    useEffect(() => {
        async function fetchData() {
            var myHeaders = new Headers();
            myHeaders.append("Authorization", "Bearer " + token);
            var requestOptions = {
                method: 'GET',
                headers: myHeaders
            };
            const response = await fetch(`https://ad3e84978530.eu.ngrok.io/api?action=list&object=likes&user=${userId}&product=${productId}`, requestOptions)
            const result = await response.json();
            setLikes(result.likes)
        }

        if (token && userId && productId) {
            fetchData()
        }
    }, [token, userId, productId])

    return (
        <>
            {likes && (likes.length >= 1 ? <button>liked</button> : <button>not liked</button>)}
        </>
    )
}

export default Like;