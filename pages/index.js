import Link from 'next/link'
import axios from 'axios';
import useSecurity from '../useSecurity'
import Like from "../components/Like"

export default function Home({ products }) {
  const { isLoggedIn, token } = useSecurity()
  return (
    <>
      <h1>All products</h1>
      {isLoggedIn && <p>INGELOGD</p>}
      <ul>
        {
          products.map(product => {
            return (
              <li key={product.id}>
                <span>{product.name}</span>
                <div>{isLoggedIn && <Like productId={product.id} />}</div>
                <Link href={`/products/${product.id}`}><a>show detail</a></Link>
              </li>
            )
          })
        }
      </ul>
      <p>Please login to be able to like products</p>
    </>
  )
}




export const getServerSideProps = async () => {
  const response = await axios.get('https://ad3e84978530.eu.ngrok.io/api?object=products&action=list')
  const products = response.data.products
  return {
    props: {
      products
    }
  }
}