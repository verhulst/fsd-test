import Axios from "axios"
import Like from '../../components/Like'
export default (props) => {
    return (
        <>
            {
                props.product && <div><h1>{props.product.name}</h1>
                    <p>{props.product.description}</p>
                    <Like productId={parseInt(props.product.id)} />
                </div>
            }
        </>
    )
}


export const getStaticPaths = async () => {
    return {
        paths: [
            { params: { id: '2' } },
            { params: { id: '3' } }
        ],
        fallback: true
    }
}
export const getStaticProps = async (ctx) => {
    const response = await Axios.get('https://ad3e84978530.eu.ngrok.io/api?object=products&action=view&key=' + ctx.params.id)
    const product = response.data.products
    console.log(product)
    return {
        props: {
            product
        }
    }
}